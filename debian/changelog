gfm (1.09~git20220826.71eaa92-1) UNRELEASED; urgency=medium

  * New upstream version 1.09~git20220826.71eaa92
  * Forward patch and remove it after being applied upstream.

 -- Andreas B. Mundt <andi@debian.org>  Fri, 26 Aug 2022 17:25:36 +0200

gfm (1.09~git20200812.752aef4-1) unstable; urgency=medium

  * Prepare for salsa-ci.
  * Extract sources for our package from upstream git automatically.
  * New upstream version 1.09~git20200812.752aef4
  * Bump Standards-Version and dh compat level (no changes needed).
  * Update patch and remove patch applied upstream.
  * Update dependencies (closes: #1017184).
  * Add licence CC0-1.0 for AppStream meta data.
  * Fix some lintian complaints.
  * Fix hardening: Add CPPFLAGS and LDFLAGS.

 -- Andreas B. Mundt <andi@debian.org>  Sun, 14 Aug 2022 17:05:49 +0200

gfm (1.08-1) unstable; urgency=medium

  * New upstream version 1.08
  * Update Vcs-* links to 'salsa.debian.org'.
  * Bump Standards-Version to 4.1.4 (no changes needed).
  * Bump compatibility level to 11.
  * Update dependencies for new upstream release.
  * Fix outdated KDE configure stuff.
  * DEP-5 copyright and update 'debian/watch'.
  * Fix appstream and desktop metadata.

 -- Andreas B. Mundt <andi@debian.org>  Sat, 19 May 2018 10:14:38 +0300

gfm (1.07-2) unstable; urgency=medium

  * Switch to secure URI for vcs-field.
  * Remove deprecated menu file.
  * Bump Standards-Version to 3.9.8 (no changes needed).

 -- Andreas B. Mundt <andi@debian.org>  Sat, 03 Dec 2016 19:34:43 +0300

gfm (1.07-1) unstable; urgency=low

  * Team maintained in Debian-Science now (closes: #678869).
    Modifications in cooperation with Albert Huang
    <alberth.debian@gmail.com>, thanks!
  * New upstream release.
  * Bump versions of libti* dependencies.
  * Add debian/watch to enable upstream release tracking.
  * Switch to packaging format 3.0 (quilt).
  * Bump 'debian/compat' to 9 and Standards-Version to 3.9.4.

 -- Andreas B. Mundt <andi@debian.org>  Sat, 10 Aug 2013 19:39:32 +0200

gfm (1.03-2) unstable; urgency=low

  * Package suggests tilp2
  * Add missing GFM team copyright
  * Bumped Standards-Version to 3.8.3

 -- Krzysztof Burghardt <krzysztof@burghardt.pl>  Sun, 16 Aug 2009 18:14:21 +0200

gfm (1.03-1) unstable; urgency=low

  * Initial release (Closes: #532606)

 -- Krzysztof Burghardt <krzysztof@burghardt.pl>  Thu, 11 Jun 2009 14:47:41 +0200
